public class Main {

    public static void main(String[] args) {

        final int LIMIT = 1000;

        int solution = 0;

        for (int i = 0; i < LIMIT; i++) {
            for (int j = 0; j < LIMIT; j++) {
                String palindrome = String.valueOf(i*j);
                if (palindrome.equals(reverse(palindrome)) && Integer.parseInt(palindrome) > solution) {
                    solution = Integer.parseInt(palindrome);
                    System.out.println(i + " * " + j + " = " + palindrome);
                }
            }
        }

        System.out.println(solution);
    }

    public static String reverse(String string) {
        StringBuilder builder = new StringBuilder();

        for (int i = string.length()-1; i > -1; i--) {
            builder.append(string.charAt(i));
        }
        return builder.toString();
    }

}
