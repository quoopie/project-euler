def prime(num):
    factors = [x for x in range(1, num+1) if num%x == 0]
    if len(factors) == 2: return True
    else: return False

primes = []
number = 1
upper = 10001
while len(primes) != upper:
    number+=1
    if prime(number):
        primes.append(number)
        print('\033[92m' + f'{round((len(primes)/upper)*100, 2)}%' + '\033[0m' + ' complete')
print(primes[upper-1])
