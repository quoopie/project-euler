import java.util.ArrayList;

public class Main {

    public static ArrayList<Integer> primes = new ArrayList<>();

    public static void main(String[] args) {

        final int LIMIT = 10001;

        double amount = 2;
        int i = 1;
        double solution = 0;

        while (amount < LIMIT) {
            int prime = (6 * i) - 1;
            if (!isMultiple(prime)) {
                solution = prime;
                primes.add(prime);
                amount++;
            }
            if (amount == LIMIT) break;
            prime = (6 * i) + 1;
            if (!isMultiple(prime)) {
                solution = prime;
                primes.add(prime);
                amount++;
            }
            i++;
            System.out.println("\u001B[32m" + Math.floor(amount/LIMIT*100) + "%" + "\u001B[0m" + " Complete");
        }

        System.out.println(solution);
    }

    public static boolean isMultiple(int number) {
        for (int prime : primes) {
            if (number % prime == 0) return true;
        }
        return false;
    }

}
