public class Main {

    public static void main(String[] args) {
        final int LIMIT = 100;

        int solution;
        int sumSquare = 0;
        int squareSum = 0;

        for (int i = 1; i <= LIMIT; i++) {
            sumSquare += i*i;
            squareSum += i;
        }

        squareSum *= squareSum;

        solution = squareSum - sumSquare;

        System.out.println(solution);
    }

}
