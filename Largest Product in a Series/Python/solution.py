with open("input.txt") as f:
    input = f.readlines()
    inputNum = ''
    for i in range(len(input)):
        input[i] = input[i].replace("\n", '')
        inputNum += input[i]

answer = 0
adjacent = 13

for i in range(adjacent-1, len(inputNum)):
    num = 1
    for j in range(1, adjacent+1): 
        num *= int(inputNum[i-j])
    if num > answer:
        answer = num
print(answer)
