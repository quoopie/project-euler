public class Main {

    public static void main(String[] args) {

        final int limit = 4000000;

        int a = 1;
        int b = 2;
        int solution = 2;

        while (a < limit && b < limit) {
            a += b;
            b += a;
            if (a % 2 == 0) solution += a;
            if (b % 2 == 0) solution += b;
        }

        System.out.println(solution);
    }

}
