# Description
I will be publishing all of my solutions for each problem into this repo. It will mainly consist of python but may contain some C# as well.
# Credit
If I could not for the life of me figure out a problem then I will look into hints on reddit and give credit to whomever actually helped me in a `CREDITS.md` file. I will also not straight steal answers. I will understand them and remake them withoutcopying them line for line. I will then explain how they helped me and what I learned to insure that I am growing my knowledge and not stealing
