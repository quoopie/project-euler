import subprocess

upper = 2000000
primes = []

for i in range(1, upper+1):
    p1 = subprocess.run(['factor', str(i)], capture_output=True, text=True)
    p1 = p1.stdout
    p1 = p1[p1.index(':')+2:].replace('\n', '')
    p1 = p1.split()
    primes += [int(x) for x in p1 if int(x) not in primes]

    print('\u001b[32m' + f'{round(((i/upper)*100), 2)}%' + '\u001b[37m' + ' complete')

# WRONG 666667333337, 12345753100123342
print(sum(primes))
