import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        final int LIMIT = 20;

        int solution = 1;
        ArrayList<Boolean> multiples = new ArrayList<>();

        for (int i = 0; i < LIMIT; i++) {
            multiples.add(false);
        }

        while (multiples.contains(false)) {
            multiples.replaceAll(value -> false);
            for (int i = 1; i <= LIMIT; i++) {
                if (solution % i == 0) {
                    multiples.set(i-1, true);
                } else {
                    solution++;
                    break;
                }
            }
        }

        System.out.println(solution);
    }

}
