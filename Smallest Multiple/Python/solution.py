number = 0
upper = 20
breakCon = [False]
while not all(breakCon):
    number += 1
    print(f'Testing {number}')
    breakCon = [True if number%x == 0 else False for x in range(1, upper+1)]
print(f'{number} is the lowest number that evenly divides between all numbers in from 1 - {upper}')
