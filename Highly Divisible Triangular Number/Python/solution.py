from colored import fore, style
nums = []
factors = []
num = 0
upper = 500

def factor(num):
    factors = [x for x in range(1, num+1) if num%x==0]
    return factors

while len(factors) < upper:
    num += 1
    nums.append(num)
    factors = factor(sum(nums))
    print(style.RESET + 'On number' + fore.ORANGE_3 + f' {sum(nums)}' + style.RESET)

print(str(sum(nums)))
