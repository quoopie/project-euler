upper = 1000

for m in range(2, upper+1):
    for n in range(1, m+1):
        a = m**2-n**2
        b = 2*n*m
        c = n**2+m**2
        if a**2+b**2 == c**2 and a+b+c >= upper:
            break
    if a**2+b**2 == c**2 and a+b+c == upper:
        break
print(a*b*c)
